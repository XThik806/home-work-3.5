// 1)

function divide(a, b) {
    if (b == 0) {
        console.log('Помилка: ділення на нуль');
        return;
    }
    let result = a / b;
    console.log(`Ваше число ${result}`);
    return result;
}

let a,b;
do{
    a=Number(prompt("Введіть ваше A:"));
}
while(isNaN(a));
do{
    b=Number(prompt("Введіть ваше B:"));
}
while(isNaN(b));
divide(a,b);

// 2)

function math(c, d)
{
    let math, math_result;
    math=prompt("Введи математичну операцію яку треба виконати(+, -, *, / або %):");
    if(math === "+")
    {
        math_result = c + d;
        console.log(math_result);
    }
    else if(math === "-")
    {
        math_result = c - d;
        console.log(math_result);
    }
    else if(math === "*")
    {
        math_result = c * d;
        console.log(math_result);
    }
    else if(math === "/")
    {
        math_result = c / d;
        console.log(math_result);
    }
    else if (math === "%")
    {
        math_result = c % d;
        console.log(math_result);
    }
    else
    alert("Такої операції не існує");
}

let c, d;
do{
    c=Number(prompt("Введіть ваше C:"));
}
while(isNaN(c));
do{
    d=Number(prompt("Введіть ваше D:"));
}
while(isNaN(d));
math(c, d);